# MySQL Extrator

* **autor:** Patrik Samko (patrik.samko@bizztreat.com)
* **created:** 16. 7. 2019

## Description
Extractor for MySQL databases. 
##### What it is used for?
Extracting the data from any MySQL Database you have credentials to the specific folder on the local machine.
##### Why did you create it?
Because we need for out BI projects in Bizztreat.
##### What the code does?
The code - specially main.py file does:
1. Load the config file, where are all neccessary information for the extractor.
2. Create the connection between the machine, where is the script running and the remote database.
3. Ask for the specific data and save them to a specific folder on the local machine as CSV. (everything is specified in the config.json file - read on)

## Requirements
Requerements for running this project or the main.py file in src folder are: 
* python3.5 or higher 
* [mysql-connector-python](https://pypi.org/project/mysql-connector-python/)

## How to use it?

#### Local use
1) Create or modify the config.json file:
	* config.json =  configuration:
    	- config file path: /config/config.json
        - check that the file is the corrent folder, if not create it
    	- before running the sript fill in the config.json file:
        	- all fields are required
        	- field "query" options:
            	1) could contain valid sql select for extracting the data you
                	like to extract
            	2) could be empty (e.g.: "query":"") - extract all data
            	3) could by all (e.g.: "all") - extract all data
      * Example of the config.json
      ```json
          {
          "$schema": "https://gitlab.com/bizzflow-extractors/ex-mysql/-/raw/master/config.schema.json",
          "user": "patrik",
          "password": "12345",
          "host": "mysql-db.cz",
          "database": "customers",
          "query": {
              "table_name": "SELECT * FROM table_name"
            }
          }
      ```
2) Run the /scr/main.py
      ```sh
      python3 main.py
      ```
#### Cloud use
##### Google Cloud Platform
1) Create the config.json file and store in the KMS.
2) Now you will use the Dockerfile, which is prepared for the Worker Machine, which run in every Bizzflow project.
3) In you project just setup the Airflow dag, which start up the Worker VM and give it an information with specified repository to clone and run with specific config from KMS.

> More information can give you Tomáš Votava (tomas.votava@bizztreat.com) - author of this logic.

## TODO
##### Things which are not completely done?
* None.
##### Things which could improve the current project/program/script?
* Maybe think about using the OOP in the main.py? 

## OTHER
* tested locally on WIN10 - run perfectly!
* tested on XOM Materials Bizzflow project - first half of 2019 - run perfectly!
