import csv
import logging
import json
import os
from mysql.connector import errorcode, errors, connect
from retry_helper import RetryManager

logger = logging.getLogger(__name__)


class MySQLExtractor:
    BUFFER_LINES = 1000

    def __init__(self, config_file_path):
        self.conf = self.load_config(config_file_path)
        self.user = self.conf["user"]
        self.password = self.conf["password"]
        self.host = self.conf["host"]
        self.database = self.conf["database"]
        self.raise_on_warnings = self.conf.get("raise_on_warnings", True)
        self.output_directory = self.conf.get("output_directory", "/data/out/tables")
        self.port = self.conf.get("port", 3306)
        self.query = self.conf["query"]
        self._connection = None
        self._cursor = None

    def load_config(self, config_path):
        """
        Loads the config file.
            :param config_path: Path to the config file (e.g.: "/config/config.json")
        """
        if not os.path.exists(config_path):
            raise Exception("Configuration not specified")

        with open(config_path) as conf_file:
            conf = json.loads(conf_file.read())
            return conf

    def create_output_directory(self, dir_path):
        """
        Create target directory & all intermediate directories if don't exists.
            :param dir_path: directory Path (e.g.: "/data/out/tables")
        """
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
            logger.info(f"Directory {dir_path} Created ")
        else:
            logger.info(f"Directory {dir_path} already exists")

    @property
    def connection(self):
        if self._connection is None:
            logger.info("Connecting to MySQL database...")
            try:
                with RetryManager(max_attempts=5, wait_seconds=1, exceptions=errors.DatabaseError) as retry:
                    while retry:
                        with retry.attempt:
                            self._connection = connect(user=self.user,
                                                       password=self.password,
                                                       host=self.host,
                                                       database=self.database,
                                                       port=self.port,
                                                       raise_on_warnings=self.raise_on_warnings,
                                                       ssl_ca="")
            except errors.Error as err:
                if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                    logger.error("Something is wrong with your user name or password")
                elif err.errno == errorcode.ER_BAD_DB_ERROR:
                    logger.error("Database does not exist")
                else:
                    logger.error(err)
                raise err
            else:
                logger.info("Connection established.")
        return self._connection

    @property
    def cursor(self):
        if self._cursor is None:
            self._cursor = self.connection.cursor()
        return self._cursor

    def close_cursor_and_connection(self):
        if self._cursor:
            try:
                self._cursor.close()
            except Exception as e:
                logger.warning(f"Cannot close cursor: {e}")
            else:
                logger.info("Cursor closed")
            finally:
                self._cursor = None
        if self._connection:
            try:
                self._connection.close()
            except Exception as e:
                logger.warning(f"Cannot close connection: {e}")
            else:
                logger.info("Connection closed")
            finally:
                self._connection = None

    def list_all_tables(self):
        logger.info("Listing all tables")
        schema = "information_schema.tables"
        query_all_tables = "SELECT TABLE_NAME FROM {} WHERE TABLE_TYPE = 'BASE TABLE';".format(schema)
        with RetryManager(max_attempts=5, wait_seconds=1, reset_func=self.close_cursor_and_connection) as retry:
            while retry:
                with retry.attempt:
                    self.cursor.execute(query_all_tables)
                    all_tables = self.cursor.fetchall()
        return all_tables

    def extract_all_tables(self):
        logger.info("Type of extraction: Extracting all tables")
        tables = self.list_all_tables()
        for table in tables:
            table = ''.join(table)  # change from tuple to string
            logger.info("Extracting table: " + table)
            query = "SELECT * FROM " + self.database + "." + table + ";"
            self.extract_query(table, query)
        logger.info("All tables extracted <3")

    def extract_query(self, file_name, query):
        with RetryManager(max_attempts=5, wait_seconds=1, reset_func=self.close_cursor_and_connection) as retry:
            while retry:
                with retry.attempt:
                    self.cursor.execute(query)
                    field_names = [i[0] for i in self.cursor.description]
                    logger.info("Column names: " + str(field_names))

                    output_filename = os.path.join(self.output_directory, file_name + ".csv")
                    logger.info("Saving output to \'{0}\'".format(output_filename))

                    with open(output_filename, 'w', newline='') as result_file:
                        wr = csv.writer(result_file, dialect=csv.unix_dialect)
                        wr.writerow(field_names)
                        buf = self.cursor.fetchmany(self.BUFFER_LINES)
                        while buf:
                            wr.writerows(buf)
                            buf = self.cursor.fetchmany(self.BUFFER_LINES)
                    logger.info("Query output extracted to \'{0}\'".format(output_filename))

    def extract(self):
        """
        Type of extract: All vs. Query
            if the value of query in config.json = "all" or  is empty("") then all tables,
            if there is a query, it will use the query
        """
        self.create_output_directory(self.output_directory)

        # Extract all tables from database
        if self.query == 'all' or self.query == "":
            self.extract_all_tables()
        # Extract by querying
        else:
            for name, query in self.query.items():
                self.extract_query(name, query)
        self.close_cursor_and_connection()
