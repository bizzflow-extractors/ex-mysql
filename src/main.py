# -*- coding: utf-8 -*-
# pylint: enable=C,R,W,E,F
# pylint: disable=unused-variable

"""
MySQL extractor

Created: 16.7.2019
Author: Patrik Samko (patrik.samko@bizztreat.com)

This script extract data from MySQL database.

Requirements: python3, mysql-connector-python
Config file:
    - config file path: /config/config.json
    - before running the sript fill in the config.json file:
        - all fields are required
        - field "query" options:
            1) could contain valid sql select for extracting the data you
                like to extract
            2) could be empty (e.g.: "query":"") - extract all data
            3) could by all (e.g.: "all") - extract all data

    Config example:
    {
    "$schema": "https://gitlab.com/bizzflow-extractors/ex-mysql/-/raw/master/config.schema.json",
	"user": "patrik",
	"password": "12345",
	"host": "mysql-db.cz",
	"database": "customers",
	"query": {
        "table_name": "SELECT column1, column2 FROM table_name"
        }
    }
Usage: python3 main.py
"""
from logging import basicConfig, INFO

from mysql_extractor.extractor import MySQLExtractor


def main():
    """
    Main function to be called if from __main__
    """
    extractor = MySQLExtractor("/config/config.json")
    extractor.extract()


if __name__ == "__main__":
    basicConfig(level=INFO, format="[{asctime}] {levelname:10} {message}", style="{")
    main()
